package com.nusrat.nusratfatima.common

import android.view.View

interface OnClickHandlerInterface<T> {

    fun onClickView(view: View,model: T)
}