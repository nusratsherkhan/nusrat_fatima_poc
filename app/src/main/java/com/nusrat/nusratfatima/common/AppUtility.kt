package com.nusrat.nusratfatima.common

import android.os.Environment
import java.io.File

class AppUtility {
    companion object{
        @JvmStatic
        fun getFilePath(): String {
            return Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).absolutePath
        }
    }
}