package com.nusrat.nusratfatima.common

import android.os.Parcel
import android.os.Parcelable
import androidx.databinding.BaseObservable

open class BaseModel() : BaseObservable(), Parcelable{

    var position = 0

    constructor(parcel: Parcel) : this() {
        readFromParcel(parcel)
    }

    open fun readFromParcel(parcel: Parcel) {

    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {

    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<BaseModel> {
        override fun createFromParcel(parcel: Parcel): BaseModel {
            return BaseModel(parcel)
        }

        override fun newArray(size: Int): Array<BaseModel?> {
            return arrayOfNulls(size)
        }
    }
}

