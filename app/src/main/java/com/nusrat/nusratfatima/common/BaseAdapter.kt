package com.nusrat.nusratfatima.common

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.nusrat.nusratfatima.databinding.DataLayoutBinding


class BaseAdapter<T : BaseModel> : RecyclerView.Adapter<BaseViewHolder> {

        private var layoutResource = 0
        var list: MutableList<T> = ArrayList()

        var handlerInterface: OnClickHandlerInterface<*>? = null


        /**
         * basic function can make use of all below method if required
         *
         * @param
         */

        val items: List<T>?
            get() = list

        val firstItem: T?
            get() = if (list.size > 0) list[0] else null

        constructor(layoutResource: Int, handlerInterface: OnClickHandlerInterface<*>?= null) {
            this.layoutResource = layoutResource
            this.handlerInterface = handlerInterface
        }

        override fun onCreateViewHolder(
            parent: ViewGroup,
            viewType: Int
        ): BaseViewHolder {
            val layoutInflater = LayoutInflater.from(parent.context)
            val binding = DataBindingUtil.inflate<DataLayoutBinding>(
                layoutInflater, viewType, parent, false
            )
            return BaseViewHolder(binding)
        }

        override fun onBindViewHolder(
            holder: BaseViewHolder,
            position: Int
        ) {
            val obj = list[position]
            holder.bind(obj, handlerInterface, position)
        }

        override fun getItemViewType(position: Int): Int {
            return layoutResource

        }

        override fun getItemCount(): Int {
            return list.size
        }

        /**
         * adding data for the first time in the list and scroll if required to last position
         *
         * @param li
         */
        open fun updateList(li: List<T>?) {
            if (li != null) {
                this.list.clear()
                this.list = ArrayList(li)
                notifyDataSetChanged()
            }
        }
    }




