package com.nusrat.nusratfatima.common

import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView
import com.nusrat.nusratfatima.BR


class BaseViewHolder(val binding: ViewDataBinding) : RecyclerView.ViewHolder(binding.root) {

    fun bind(obj: Any,handlerInterface: OnClickHandlerInterface<*>?, position: Int) {
        if (obj is BaseModel){
            obj.position = position
        }
        binding.setVariable(BR.obj, obj)
        binding.setVariable(BR.handler,handlerInterface)
        binding.executePendingBindings()
    }

}