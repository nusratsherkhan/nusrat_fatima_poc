package com.nusrat.nusratfatima.common

import android.content.Intent
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.AppCompatTextView
import androidx.appcompat.widget.Toolbar
import com.nusrat.nusratfatima.R

abstract class BaseActivity : AppCompatActivity() {

    var toolbarTitle: AppCompatTextView? = null

    open fun getToolBar(): Toolbar? {
        return findViewById(R.id.toolbar)
    }

    override fun setContentView(layoutResID: Int) {
        super.setContentView(layoutResID)
        initSetup()
    }

    override fun setContentView(view: View?) {
        super.setContentView(view)
        initSetup()
    }

    override fun setContentView(view: View?, params: ViewGroup.LayoutParams?) {
        super.setContentView(view, params)
        initSetup()
    }

    protected fun initSetup() {
        initToolBar(getToolBar())

    }

    private fun initToolBar(toolBar: Toolbar?) {
        toolBar?.run {
            toolbarTitle = findViewById(R.id.toolbar_title)
            setSupportActionBar(toolBar)
            getDrawableBack()?.let {
                toolBar.setNavigationIcon(it)
            }
            setSupportActionBar(toolBar)
            supportActionBar?.setHomeButtonEnabled(true)
            invalidateOptionsMenu()
        }
    }

    open fun getDrawableBack(): Int? {
        return R.drawable.back_icon
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                handleHomeButtonClick()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun handleHomeButtonClick() {
        if (supportFragmentManager.backStackEntryCount > 0)
            supportFragmentManager.popBackStack()
        else
            finish()
    }

    fun setPageTitle(title: String) {
        toolbarTitle?.let { it.text = title }
    }

}