package com.nusrat.nusratfatima.service

import android.app.DownloadManager
import android.app.IntentService
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Environment

class DownloadService : IntentService("DownloadService") {
    override fun onHandleIntent(intent: Intent?) {
        intent?.let {
            val url = it.getStringExtra("keykey")
            val downloadManager = getSystemService(Context.DOWNLOAD_SERVICE) as DownloadManager
            val uri = Uri.parse(url)
            val request = DownloadManager.Request(uri)
            request.setDestinationInExternalPublicDir(
                Environment.DIRECTORY_DOWNLOADS,
                uri.lastPathSegment
            )
            downloadManager.enqueue(request)
        }

    }
}