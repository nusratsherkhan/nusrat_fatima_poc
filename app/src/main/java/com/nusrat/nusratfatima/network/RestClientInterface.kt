package com.nusrat.nusratfatima.network

import io.reactivex.Observable
import retrofit2.Response
import retrofit2.http.GET

interface RestClientInterface {

    @GET("audio")
    fun getData() : Observable<Response<ResponseModelClass>>

}
