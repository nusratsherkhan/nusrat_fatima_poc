package com.nusrat.nusratfatima.network

import com.nusrat.nusratfatima.ui.model.DataModelClass
import com.nusrat.nusratfatima.common.BaseModel

data class ResponseModelClass(val data: ArrayList<DataModelClass>): BaseModel()


