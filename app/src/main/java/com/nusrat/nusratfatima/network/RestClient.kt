package com.nusrat.nusratfatima.network

import com.google.gson.GsonBuilder
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

class RestClient{

    companion object {

        @JvmStatic
        private var restservice: RestClientInterface? = null

        @JvmStatic
        fun getClient() : RestClientInterface {
            if (restservice ==null){
                val gson = GsonBuilder()
                    .setDateFormat("yyyy-MM-dd'T'HH:mm:ssZ")
                    .create()
                val retrofit = Retrofit.Builder().baseUrl("https://demo0701212.mockable.io/")
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .build()

                restservice = retrofit.create(
                    RestClientInterface::class.java)
                return restservice!!
            }
            else{
                return restservice!!
            }

        }
    }

    private constructor() }

