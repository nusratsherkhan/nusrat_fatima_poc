package com.nusrat.nusratfatima.util;

import android.app.Activity;
import android.content.pm.PackageManager;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

public class PermissionUtility {

    public static boolean checkPermission(Activity activity, String perm, boolean askUser, int requestCode) {
        int permissionCheck = ContextCompat.checkSelfPermission(activity, perm);
        if (permissionCheck == PackageManager.PERMISSION_GRANTED) {
            // We have the permission
            return true;
        } else if (askUser) {
            // No explanation needed, we can request the permission.
            String[] permissions = new String[]{perm};
            ActivityCompat.requestPermissions(activity, permissions, requestCode);
            return false;
        }
        return false;
    }

    public static boolean checkPermission(Fragment fragment, String perm, boolean askUser, int requestCode) {
        int permissionCheck = ContextCompat.checkSelfPermission(fragment.getContext(), perm);
        if (permissionCheck == PackageManager.PERMISSION_GRANTED) {
            // We have the permission
            return true;
        } else if (askUser) {
            // No explanation needed, we can request the permission.
            String[] permissions = new String[]{perm};
            fragment.requestPermissions(permissions, requestCode);
            return false;
        }
        return false;
    }

}