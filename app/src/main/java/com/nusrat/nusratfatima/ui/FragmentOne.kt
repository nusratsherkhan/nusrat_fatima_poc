package com.nusrat.nusratfatima.ui

import android.app.DownloadManager
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.media.MediaPlayer
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import com.nusrat.nusratfatima.R
import com.nusrat.nusratfatima.common.AppUtility
import com.nusrat.nusratfatima.databinding.FragmentOneLayoutBinding
import com.nusrat.nusratfatima.service.DownloadService
import com.nusrat.nusratfatima.ui.model.PlayerActivityModel
import java.io.File
import java.io.IOException


class FragmentOne : Fragment() {

    private val TAG = this::class.java.simpleName

    lateinit var binding: FragmentOneLayoutBinding
    lateinit var model: PlayerActivityModel
    lateinit var mp: MediaPlayer
    var inQueue: String = ""

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_one_layout, container, false
        )
        binding.fragmentImg1.setAnimation("anims/" + "stream.json")
        binding.fragmentImg1.repeatCount = 10000
        return binding.root
    }

    fun playAnimation() {
        binding.fragmentImg1.playAnimation()
    }

    fun stopAnimation(){
        binding.fragmentImg1.clearAnimation()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        model = arguments!!.getParcelable<PlayerActivityModel>("key")!!
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mp = MediaPlayer()

        playCurrentAudio(model)

        binding.fragmentBtn1.setOnClickListener {
            stopAudio()
            val position = model.positionToPlay + 1
            if (position > model.list.size - 1) {
                stopAudio()
                val intent = Intent(activity, MainActivity::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK
                startActivity(intent)
                activity!!.finish()
            } else {
                playCurrentAudio(model.apply {
                    positionToPlay = position
                })
            }
        }
    }

    private fun playCurrentAudio(model: PlayerActivityModel) {
        binding.obj = model.list[model.positionToPlay]
        val url = model.list[model.positionToPlay].audio
        val path = getFilePath(url)
        val file = File(path)
        if (file.exists())
            playAudio(url)
        else {
            binding.fragmentBtn1.isEnabled = false
            binding.fragmentBtn1.text = getString(R.string.buffering)
            activity!!.registerReceiver(
                onComplete,
                IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE)
            )
            downLoadAudio(url)
        }
    }

    fun getFilePath(url: String): String {
        val uri = Uri.parse(url)
        return AppUtility.getFilePath() + "/" + uri.lastPathSegment
    }

    private fun downLoadAudio(url: String) {

        inQueue = url

        val intent = Intent(context, DownloadService::class.java).apply {
            putExtra("keykey", url)
        }
        context?.startService(intent)
    }

    var onComplete: BroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(ctxt: Context, intent: Intent) {
            Log.d(TAG, "downloaded")
            playAudio(inQueue)
        }
    }

    fun stopAudio() {
        mp.stop()
        stopAnimation()

    }

    fun playAudio(path: String) {

        mp.reset()

        val pathToPlay = getFilePath(path)
        // val file = File(path)
        //set up MediaPlayer
        binding.fragmentBtn1.isEnabled = true
        binding.fragmentBtn1.text = getString(R.string.continu)

        try {
            mp.setDataSource("$pathToPlay")
        } catch (e: IllegalArgumentException) {
            // TODO Auto-generated catch block
            e.printStackTrace()
        } catch (e: IllegalStateException) {
            // TODO Auto-generated catch block
            e.printStackTrace()
        } catch (e: IOException) {
            // TODO Auto-generated catch block
            e.printStackTrace()
        }

        try {
            mp.prepare()
        } catch (e: IllegalStateException) {
            // TODO Auto-generated catch block
            e.printStackTrace()
        } catch (e: IOException) {
            // TODO Auto-generated catch block
            e.printStackTrace()
        }

        mp.start()
        playAnimation()

        downloadNextAudio()
        Log.d(TAG, mp.duration.toString())
    }

    private fun downloadNextAudio() {
        val position = model.positionToPlay + 1
        if (position > model.list.size-1) {

        } else {
            val url = model.list[position].audio
            val path = getFilePath(url)
            val file = File(path)
            if (!file.exists())
                downLoadAudio(url)
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        stopAudio()
    }

    companion object {

        fun getInstance(playerActivityModel: PlayerActivityModel): FragmentOne {

            val fragment = FragmentOne()
            val bundle = Bundle()
            bundle.putParcelable("key", playerActivityModel)
            fragment.arguments = bundle
            return fragment

        }
    }
}