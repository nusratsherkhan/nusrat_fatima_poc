package com.nusrat.nusratfatima.ui

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.nusrat.nusratfatima.R
import com.nusrat.nusratfatima.common.AppUtility
import com.nusrat.nusratfatima.common.BaseActivity
import com.nusrat.nusratfatima.common.BaseAdapter
import com.nusrat.nusratfatima.common.OnClickHandlerInterface
import com.nusrat.nusratfatima.databinding.ActivityMainBinding
import com.nusrat.nusratfatima.service.DownloadService
import com.nusrat.nusratfatima.ui.model.DataModelClass
import com.nusrat.nusratfatima.ui.model.PlayerActivityModel
import com.nusrat.nusratfatima.ui.viewmodel.ViewModelClass
import java.io.File


class MainActivity : BaseActivity(), OnClickHandlerInterface<DataModelClass> {
    override fun onClickView(view: View, model: DataModelClass) {

        val intent = Intent(this, PlayerActivity::class.java)
        val model = PlayerActivityModel(model.position, vm.liveData.value!!)
        intent.putExtra("key", model)
        startActivity(intent)
    }

    private val TAG = this::class.java.simpleName

    lateinit var vm: ViewModelClass

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)


        val binding =
            DataBindingUtil.setContentView<ActivityMainBinding>(
                this,
                R.layout.activity_main
            )

        setPageTitle(getString(R.string.audio_list))

        val adapter = BaseAdapter<DataModelClass>(R.layout.data_layout, this)

        binding.recyclerView.layoutManager = LinearLayoutManager(this)
        binding.recyclerView.adapter = adapter

        vm = ViewModelProviders.of(this).get(ViewModelClass::class.java)
        vm.getData()

        vm.liveData.observe(this, androidx.lifecycle.Observer {
            binding.progress.visibility = View.GONE
            adapter.updateList(it)
            if (it.size > 0)
                downLoadAudio(it[0].audio)
        })

    }

    override fun getDrawableBack(): Int? {
        return null
    }

    private fun downLoadAudio(url: String) {
        val path = AppUtility.getFilePath() + "/" + Uri.parse(url).lastPathSegment
        val file = File(path)
        if (!file.exists()) {
            val intent = Intent(this, DownloadService::class.java).apply {
                putExtra("keykey", url)
            }
            startService(intent)
        }
    }
}
