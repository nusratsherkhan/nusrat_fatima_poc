package com.nusrat.nusratfatima.ui

import android.os.Bundle
import com.nusrat.nusratfatima.R
import com.nusrat.nusratfatima.common.BaseActivity
import com.nusrat.nusratfatima.ui.model.PlayerActivityModel

class PlayerActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.player_activity_layout)

        setPageTitle(getString(R.string.shuffle_play))

        val model=intent.getParcelableExtra<PlayerActivityModel>("key")

        val fm = supportFragmentManager
        fm.beginTransaction().add(R.id.container,FragmentOne.getInstance(model), "fragment1")
            .commit()
    }

}