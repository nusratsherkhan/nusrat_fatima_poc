package com.nusrat.nusratfatima.ui.viewmodel

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.nusrat.nusratfatima.ui.model.DataModelClass
import com.nusrat.nusratfatima.network.ResponseModelClass
import com.nusrat.nusratfatima.network.RestClient
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.observers.DisposableObserver
import io.reactivex.schedulers.Schedulers
import retrofit2.Response

class ViewModelClass : ViewModel() {

    // this live data would be observed in Activity
    var liveData = MutableLiveData<List<DataModelClass>>()

    init {

    }

    fun getData() {

        // Rest client to call api

        //later we can create an extension for this so that boiler plate code can be avoided.
            RestClient.getClient().getData()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : DisposableObserver<Response<ResponseModelClass>>(){
                    override fun onComplete() {
                    }

                    override fun onNext(t: Response<ResponseModelClass>) {
                        if (t.isSuccessful)
                            t.body()?.let { handleResults(it) }
                    }

                    override fun onError(e: Throwable) {
                       handleError(e)
                    }

                })

    }

    fun handleResults(response: ResponseModelClass) {
        liveData.value = response.data
    }

    fun handleError(t: Throwable) {
        Log.d("API Failure", "error")
    }
}
