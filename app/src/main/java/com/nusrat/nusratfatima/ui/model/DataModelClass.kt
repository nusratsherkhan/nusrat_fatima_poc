package com.nusrat.nusratfatima.ui.model

import com.nusrat.nusratfatima.common.BaseModel
import kotlinx.android.parcel.Parcelize

@Parcelize
data class DataModelClass(val itemId: String, val desc : String, val audio: String): BaseModel()