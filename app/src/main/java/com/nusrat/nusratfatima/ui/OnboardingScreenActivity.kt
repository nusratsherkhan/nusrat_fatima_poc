package com.nusrat.nusratfatima.ui

import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import com.nusrat.nusratfatima.R
import com.nusrat.nusratfatima.common.BaseActivity
import com.nusrat.nusratfatima.databinding.OnboardingScreenLayoutBinding
import com.nusrat.nusratfatima.util.PermissionUtility
import java.util.jar.Manifest

class OnboardingScreenActivity : BaseActivity() {

    private val TAG = this::class.java.simpleName

    lateinit var binding: OnboardingScreenLayoutBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = DataBindingUtil.setContentView<OnboardingScreenLayoutBinding>(
            this,
            R.layout.onboarding_screen_layout
        )
        playAnimation()
        binding.btn.setOnClickListener {
            if (PermissionUtility.checkPermission(
                    this@OnboardingScreenActivity,
                    android.Manifest.permission.READ_EXTERNAL_STORAGE,
                    true,
                    1
                )
            ) {
                startNextActivity()
            }
        }
    }

    fun startNextActivity(){
        val intent = Intent(this, MainActivity::class.java)
        startActivity(intent)
        finish()
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
            startNextActivity()
        } else{finish()}
    }

    fun playAnimation() {
        binding.lottieWelcomeLayer.setAnimation("anims/" + "welcome1.json")
        binding.lottieWelcomeLayer.repeatCount = 10000
        binding.lottieWelcomeLayer.playAnimation()
    }
}