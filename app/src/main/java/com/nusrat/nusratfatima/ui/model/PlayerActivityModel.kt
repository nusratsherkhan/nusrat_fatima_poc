package com.nusrat.nusratfatima.ui.model

import com.nusrat.nusratfatima.common.BaseModel
import kotlinx.android.parcel.Parcelize


@Parcelize
data class PlayerActivityModel(var positionToPlay: Int, var list: List<DataModelClass>): BaseModel(){
}